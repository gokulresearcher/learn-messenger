<?php

declare(strict_types=1);

namespace App\Controller;

use App\ManageShopping\AddShoppingItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShoppingController extends AbstractController
{
    /**
     * @Route(path="/add-items", methods={"POST"})
     */
    public function addShoppingItems(Request $request): Response
    {
        $item = new AddShoppingItem();
        $item->setIteam($request->get('item'));

        $this->dispatchMessage($item);

        return new Response("Item added to shoping cart");
    }

    /**
     * @Route(path="/get-item/{item}", methods={"GET"})
     */
    public function getShoppingItem(string $item): Response
    {
        return new Response('Item:'. $item);
    }
}